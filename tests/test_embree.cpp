// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU Lesser General Public License
// as published by the Free Software Foundation; either version 2.1
// of the License, or (at your option) any later version.

// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU Lesser General Public License for more details.

// You should have received a copy of the GNU Library General Public
// License along with this library; if not, write to the
// Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
// Boston, MA 02110-1301, USA.

#include "varays/Context/VaraysContext.hpp"
#include "varays/Encoder/VaraysEncoder.hpp"
#include <vector>

int main()
{
    varays::VaraysContext context("", Eigen::Vector3f(0.0, 0.0, 0.0), false);

    const std::vector<float> frequencies = {125, 250, 500, 1000, 2000, 4000, 8000, 16000};
    varays::VaraysEncoder encoder(context.getFrequencies(), 48000);
    encoder.setAtmosphericConditions(50.0, 101.325, 20);
    context.readObjFile("./check_cubeRoom.obj", false);

    context.addSource(Eigen::Vector3f(2.0, 2.0, 2.0), "test_source1");
    context.moveListener(Eigen::Vector3f(-2.0, -2.0, -2.0));

    context.addRayManager(0, 4, 1, 0.005);
    std::vector<SoundEvent> soundEvents = context.executeRayManagers(100, 4, 3);

    std::map<unsigned int, std::vector<float>> IRForSources = encoder.writeIR(0, soundEvents);

    encoder.exportIR(IRForSources[0], "testIR", 1, 48000);
    return (0);
}