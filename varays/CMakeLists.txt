add_library(${VARAYS_LIBRARY} SHARED
    Auralizer/zita-convolver/zita-convolver.cpp
    Auralizer/jclient.cpp
    Auralizer/DopplerEngine/DelayLine.cpp
    Auralizer/DopplerEngine/DopplerEngine.cpp
    Auralizer/VaraysAuralizer.cpp
    Common/AudioMat.cpp
    Common/AudioSource.cpp
    Common/SoundEvent.cpp
    Context/Scene/ObjLoader.cpp
    Context/RayTracer/EmbreeManager.cpp
    Context/RayTracer/RayManager.cpp
    Context/RayTracer/utils.cpp
    Context/VaraysContext.cpp
    Encoder/Ambisonics.cpp
    Encoder/LRTree.cpp
    Encoder/VaraysEncoder.cpp
    Utils/Logger.cpp
    )

set(HEADER_INCLUDES
    Auralizer/zita-convolver/zita-convolver.hpp
    Auralizer/jclient.hpp
    Auralizer/prbsgen.hpp
    Auralizer/DopplerEngine/DelayLine.hpp
    Auralizer/DopplerEngine/DopplerEngine.hpp
    Auralizer/VaraysAuralizer.hpp
    Common/AudioMat.hpp
    Common/AudioSource.hpp
    Common/Energy.hpp
    Common/SoundEvent.hpp
    Context/Scene/3D-structs.hpp
    Context/Scene/GeomData.hpp
    Context/Scene/ObjLoader.hpp
    Context/RayTracer/EmbreeManager.hpp
    Context/RayTracer/RayManager.hpp
    Context/RayTracer/RootNode.hpp
    Context/RayTracer/TreeNode.hpp
    Context/RayTracer/DiffractionData.hpp
    Context/RayTracer/utils.hpp
    Context/VaraysContext.hpp
    Encoder/Ambisonics.hpp
    Encoder/LRTree.hpp
    Encoder/VaraysEncoder.hpp
    Utils/Spinlock.hpp
    Utils/Timer.hpp
    Utils/periodic-task.hpp
    Utils/Logger.hpp
    Utils/JsonSave.hpp
    Utils/Matrix3d.hpp
    Utils/Matrix4d.hpp
    )

if (OSX)
    target_link_libraries(${VARAYS_LIBRARY} PRIVATE resolv)
endif()

set_target_properties(${VARAYS_LIBRARY} PROPERTIES VERSION ${VARAYS_VERSION_STRING} SOVERSION ${VARAYS_VERSION_MAJOR})

target_link_libraries(${VARAYS_LIBRARY}
    PRIVATE ${COMMON_LIBRARIES}
    )

target_include_directories(${VARAYS_LIBRARY}
    PRIVATE ${COMMON_INCLUDE_DIRS}
    )

add_definitions(-DENABLE_VECTOR_MODE)

# Pkg-config
if (UNIX)
    include(FindPkgConfig QUIET)
    if (PKG_CONFIG_FOUND)
        configure_file("varays.pc.in" "varays-${VARAYS_API_VERSION}.pc" @ONLY)
        install(FILES "${CMAKE_CURRENT_BINARY_DIR}/varays-${VARAYS_API_VERSION}.pc"
            DESTINATION "${CMAKE_INSTALL_PREFIX}/lib/pkgconfig")
    endif ()
endif ()

install(TARGETS ${VARAYS_LIBRARY}
    LIBRARY DESTINATION lib
    COMPONENT libraries)

install(FILES ${HEADER_INCLUDES}
    DESTINATION include/varays-${VARAYS_API_VERSION}/varays
    COMPONENT headers
    )